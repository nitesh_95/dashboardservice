package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UbiBmDashboardServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UbiBmDashboardServiceApplication.class, args);
	}

}
 