package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Loanorigination")
public class Loanorigination {
	@Id
	@Column(name = "Applicationid")
	private String applicationid;
	@Column(name="solid")
	private String solid;
	@Column(name = "Bankupdateddatetime")
	private String bankupdateddatetime;
	@Column(name="Bankverificationstatus")
	private String bankverificationstatus;
	
	public String getApplicationid() {
		return applicationid;
	}
	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}
	public String getBankupdateddatetime() {
		return bankupdateddatetime;
	}
	public void setBankupdateddatetime(String bankupdateddatetime) {
		this.bankupdateddatetime = bankupdateddatetime;
	}
	public String getBankverificationstatus() {
		return bankverificationstatus;
	}
	public void setBankverificationstatus(String bankverificationstatus) {
		this.bankverificationstatus = bankverificationstatus;
	}
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	@Override
	public String toString() {
		return "Loanorigination [applicationid=" + applicationid + ", solid=" + solid + ", bankupdateddatetime="
				+ bankupdateddatetime + ", bankverificationstatus=" + bankverificationstatus + "]";
	}
	
	
	
}
