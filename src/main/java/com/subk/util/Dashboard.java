package com.subk.util;


public class Dashboard {
    
    private int approvedCount;
    
    private int pendingCount;
    private int returnedCount;
    private int rejectedCount;
	public int getApprovedCount() {
		return approvedCount;
	}
	public void setApprovedCount(int approvedCount) {
		this.approvedCount = approvedCount;
	}
	public int getPendingCount() {
		return pendingCount;
	}
	public void setPendingCount(int pendingCount) {
		this.pendingCount = pendingCount;
	}
	public int getReturnedCount() {
		return returnedCount;
	}
	public void setReturnedCount(int returnedCount) {
		this.returnedCount = returnedCount;
	}
	public int getRejectedCount() {
		return rejectedCount;
	}
	public void setRejectedCount(int rejectedCount) {
		this.rejectedCount = rejectedCount;
	}
	@Override
	public String toString() {
		return "Dashboard [approvedCount=" + approvedCount + ", pendingCount=" + pendingCount + ", returnedCount="
				+ returnedCount + ", rejectedCount=" + rejectedCount + "]";
	}
    

   
}
