package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.Loanorigination;
import com.subk.util.Dashboard;

@Repository
public interface FetchDashboardDataRepository extends JpaRepository<Loanorigination, String> {

	@Query("select count(applicationid) from Loanorigination where solid=?1 and bankverificationstatus='pending'")
	int getDashboardPendingDetails1(String solid);

	@Query(value = "select count(ApplicationId) from Loanorigination where solid=?1 and bankverificationstatus='Approved' and left(BankUpdatedDatetime ,8) between (?2) and (?3)", nativeQuery = true)
	int getDashboardApprovedDetails(String solid, String from_date, String to_date);
 
	@Query(value = "select count(ApplicationId) from Loanorigination where solid=?1 and bankverificationstatus='Rejected' and left(BankUpdatedDatetime,8) between (?2) and (?3)", nativeQuery = true)
	int getDashboardRejectedDetails(String solid, String from_date, String to_date);

	@Query(value = "select count(ApplicationId) from Loanorigination where solid=?1 and bankverificationstatus='Returned' and left(BankUpdatedDatetime,8) between (?2) and (?3)", nativeQuery = true)
	int getDashboardReturnedDetails(String solid, String from_date, String to_date);

	@Query(value = "select count(ApplicationId) from Loanorigination where solid=?1 and bankverificationstatus in('Pending') or bankverificationstatus is null  and left(UhUpdatedDatetime,8) between (?2) and (?3)", nativeQuery = true)
	int getDashboardPendingDetails(String solid, String from_date, String to_date);
	
	@Query("select count(applicationid) from Loanorigination where solid=?1 and bankverificationstatus='Approved'")
	int getDashboardApprovedDetails1(String solid);

	@Query("select count(applicationid) from Loanorigination where solid=?1 and bankverificationstatus='Rejected'")
	int getDashboardRejectedDetails1(String solid);

	@Query("select count(applicationid) from Loanorigination where solid=?1 and bankverificationstatus='Returned'")
	int getDashboardReturnedDetails1(String solid);
	
	
}
