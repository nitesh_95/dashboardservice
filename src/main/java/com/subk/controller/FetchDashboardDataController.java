package com.subk.controller;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.subk.services.FetchDashboardDataServiceImpl;
import com.subk.util.Dashboard;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class FetchDashboardDataController {
	Logger log = LoggerFactory.getLogger(FetchDashboardDataController.class);
	@Autowired
	FetchDashboardDataServiceImpl fetchDashboardDataServiceImpl;

	@RequestMapping("/dashboard")
	public Dashboard gettingDashboardData(@RequestParam(name = "From_Date") String from_date,
			@RequestParam(name = "To_Date") String to_date, @RequestParam(name = "solId") String solid) {
		log.info("SolId:::::::::" + solid);
		log.info("From_Date:::::" + from_date);
		log.info("To_Date::::::::" + to_date);
		Dashboard dashboard = fetchDashboardDataServiceImpl.getDashboardDetails(from_date, to_date, solid);

		return dashboard;
	}

}
