package com.subk.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.subk.repository.FetchDashboardDataRepository;
import com.subk.util.Dashboard;

@Repository
public class FetchDashboardDataDao {

	@Autowired
	FetchDashboardDataRepository fetchDashboardDataRepository;
	
	public Dashboard getDashboardDetails(String from_date,String to_date,String solid) {
		int pencount,appcount,rejeccount,returncount;
		Dashboard d = new Dashboard();
		
		 pencount =  fetchDashboardDataRepository.getDashboardPendingDetails(solid,from_date, to_date);
		 d.setPendingCount(pencount);
		
		 appcount = fetchDashboardDataRepository.getDashboardApprovedDetails(solid,from_date, to_date);
		 d.setApprovedCount(appcount);
		
		 rejeccount = fetchDashboardDataRepository.getDashboardRejectedDetails(solid,from_date, to_date);
		 d.setRejectedCount(rejeccount);
		
		 returncount = fetchDashboardDataRepository.getDashboardReturnedDetails(solid,from_date, to_date);
		 d.setReturnedCount(returncount);
		return d;
	}

	public Dashboard getDashboardDetails1(String solid) {
		int pencount,appcount,rejeccount,returncount;
		Dashboard d = new Dashboard();
		try {
		 pencount =  fetchDashboardDataRepository.getDashboardPendingDetails1(solid);
		 d.setPendingCount(pencount);
		 appcount = fetchDashboardDataRepository.getDashboardApprovedDetails1(solid);
		 d.setApprovedCount(appcount);
		 rejeccount = fetchDashboardDataRepository.getDashboardRejectedDetails1(solid);
		 d.setRejectedCount(rejeccount);
		 returncount = fetchDashboardDataRepository.getDashboardReturnedDetails1(solid);
	     d.setReturnedCount(returncount);
		}catch(Exception e) {
			e.printStackTrace();
		}
		 return d;
	}
}
