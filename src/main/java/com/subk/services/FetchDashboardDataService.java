package com.subk.services;

import com.subk.util.Dashboard;

public interface FetchDashboardDataService {
	public Dashboard getDashboardDetails(String fromdate,String todate,String solid);
}
