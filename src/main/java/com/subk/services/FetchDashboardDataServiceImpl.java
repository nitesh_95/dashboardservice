package com.subk.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subk.dao.FetchDashboardDataDao;
import com.subk.util.Dashboard;

@Service
public class FetchDashboardDataServiceImpl implements FetchDashboardDataService {
	
	@Autowired
	FetchDashboardDataDao FetchDashboardDatadao;
	public Dashboard getDashboardDetails(String fromdate,String todate,String solid) {
		Dashboard data = new Dashboard();
        try {
        	if((fromdate.equals("null") && todate.equals("null")) || (fromdate==null && todate==null) || (fromdate.equals("") && todate.equals(""))) {
        		data = FetchDashboardDatadao.getDashboardDetails1(solid);
        	}
        	else {
        		Date from = null, to = null;
                from = convertDateFormate(fromdate);
                to = convertDateFormate(todate);
        	    SimpleDateFormat serverFormat = new SimpleDateFormat("yyyyMMdd");
        	    System.out.println("From Date:"+serverFormat.format(from)+"   To Date::: "+serverFormat.format(to));
                data = FetchDashboardDatadao.getDashboardDetails(serverFormat.format(from), serverFormat.format(to), solid);
        	}
        }catch(Exception ex) {
        	ex.getMessage();
        }
        return data;
	}
	
	public Date convertDateFormate(String date) {
		Date date1=null;
		try {
			 date1=new SimpleDateFormat("dd/MM/yyyy").parse(date);  
		}catch(Exception e){
			e.getStackTrace();
		}
		return date1;
	}

}
